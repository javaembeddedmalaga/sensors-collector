package collector.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import collector.store.InvalidRecordIdException;
import collector.store.PersistentStore;

/** Process requests for data from a client. */
public class DataConnection implements Runnable {
    private static final Logger log = Logger.getLogger(DataServer.class.getName());
    public static final String READ_DATA = "READ";
    public static final String DISCONNECT = "DISCONNECT";

    private final PersistentStore store;
    private final InputStream input;
    private final BufferedWriter output;
    private boolean running = true;

    public DataConnection(PersistentStore store, Socket connection)
            throws IOException {
        log.info("DataConnection: in constructor");
        this.store = store;
        input = connection.getInputStream();
        output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
    }

    public void stopThread() {
        running = false;
    }

    @Override
    public void run() {
        byte buffer[] = new byte[128];
        String command;

        log.info("DataConnection: task running");

        while (running) {
            try {
                /* Read a command from the client */
                int commandLength = input.read(buffer);
                command = new String(buffer, 0, commandLength);
                log.info("command received: " + command);

                /* Process the command */
                if (command.compareTo(READ_DATA) == 0) {
                    /* Send formatted data to the client */
                    log.info("sending: " + store.getRecordCount() + " records");
                    // Send the number of message strings first
                    output.write(Integer.toString(store.getRecordCount()));
                    output.newLine();
                    // Send all the messages
                    for (int i = 1; i <= store.getRecordCount(); i++) {
                        try {
                            output.write(new String(store.getRecord(i)));
                        } catch (InvalidRecordIdException ex) {
                            log.log(Level.SEVERE, "Error witting record", ex);
                        }
                        output.newLine();
                    }
                    output.flush();
                    // break;
                } else if (command.compareTo(DISCONNECT) == 0) {
                    break;
                } else {
                    log.log(Level.SEVERE, "Unrecognized command [" + command + "]");
                }
            } catch (IOException exception) {
                log.log(Level.SEVERE, "Client I/O error", exception);
                break;
            }
        }
    }

}
