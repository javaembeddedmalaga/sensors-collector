package collector.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import collector.store.PersistentStore;

/** Simple network server to provide data from sensors. */
public class DataServer implements Runnable {
    private static final Logger log = Logger.getLogger(DataServer.class.getName());

    private final PersistentStore store;
    private final ServerSocket server;
    private boolean running = true;

    public DataServer(int port, PersistentStore store) {
        this.store = store;
        try {
            this.server = new ServerSocket(port);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /** Terminate the thread gracefully */
    public void stop() {
        running = false;
    }

    /** Code to execute in a separate thread. */
    @Override
    public void run() {
        log.info("DataServer running...");
        while (running) {
            try {
                final Socket accept = server.accept();
                log.info("Connection received");
                DataConnection connection = new DataConnection(store, accept);
                new Thread(connection).start();
            } catch (IOException exception) {
                log.log(Level.SEVERE, "IO Error accepting connection from client", exception);
            }
        }
    }

}
