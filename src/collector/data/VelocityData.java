package collector.data;

/** Current velocity of the GPS sensor. */
public class VelocityData implements SensorData {
    private final long timestamp;
    private final double trueTrack;
    private final double groundSpeed;

    public VelocityData(long timestamp, double trueTrack, double groundSpeed) {
        this.timestamp = timestamp;
        this.trueTrack = trueTrack;
        this.groundSpeed = groundSpeed;
    }

    /** Get the track for the GPS sensor (i.e. a bearing). */
    public double getTrueTrack() {
        return trueTrack;
    }

    /** Get the velocity of the GPS sensor (in Km/h). */
    public double getGroundSpeed() {
        return groundSpeed;
    }

    /** Get the time stamp for when this data was recorded. */
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Velocity{timestamp=" + timestamp + ", trueTrack=" + trueTrack + ", groundSpeed=" + groundSpeed + '}';
    }
}
