package collector.data;

public class PressureData implements SensorData {
    private final long timestamp;
    private final double pressure;

    public PressureData(long timestamp, double pressure) {
        this.timestamp = timestamp;
        this.pressure = pressure;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    /** Read the barometric pressure (in hPa) from the device. */
    public double getPressure() {
        return pressure;
    }
}
