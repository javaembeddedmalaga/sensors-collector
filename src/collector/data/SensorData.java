package collector.data;

/** Interface to define the basic format of data obtained from the embedded device. */
public interface SensorData {

    /** All device data has a timestamp associated with when the data was collected. */
    public long getTimestamp();

}
