package collector.data;

/** Data from a GPS sensor. */
public class GpsData implements SensorData {
    private final long timestamp;
    private final PositionData positionData;
    private final VelocityData velocityData;

    public GpsData(long timestamp, PositionData positionData, VelocityData velocityData) {
        this.timestamp = timestamp;
        this.positionData = positionData;
        this.velocityData = velocityData;
    }

    /** Get the position of the sensor as a latitude and longitude. */
    public PositionData getPositionData() {
        return positionData;
    }

    /** Get the velocity of the sensor (as bearing and speed). */
    public VelocityData getVelocityData() {
        return velocityData;
    }

    /** Get the time stamp for when this data was collected. */
    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "GPSData{timestamp=" + timestamp + ", position=" + positionData + ", velocity=" + velocityData + '}';
    }
}
