package collector.data;

/** The current position of the GPS sensor. */
public class PositionData implements SensorData {
    private final long timestamp;
    private final double latitude;
    private final char latitudeDirection;
    private final double longitude;
    private final char longitudeDirection;
    private final double altitude;

    public PositionData(long time, double latitude, char latitudeDirection,
                        double longitude, char longitudeDirection, double altitude) {
        this.timestamp = time;
        this.latitude = latitude;
        this.latitudeDirection = latitudeDirection;
        this.longitude = longitude;
        this.longitudeDirection = longitudeDirection;
        this.altitude = altitude;
    }

    /** Get the time this position was recorded. */
    public long getTimestamp() {
        return timestamp;
    }

    /** Get the current latitude. */
    public double getLatitude() {
        return latitude;
    }

    /** Get the direction of latitude (N or S). */
    public char getLatitudeDirection() {
        return latitudeDirection;
    }

    /** Get the longitude. */
    public double getLongitude() {
        return longitude;
    }

    /** Get the longitude direction (E or W). */
    public char getLongitudeDirection() {
        return longitudeDirection;
    }

    /** Get the altitude (in metres above sea level). */
    public double getAltitude() {
        return altitude;
    }

    @Override
    public String toString() {
        return "Position{timeStamp=" + timestamp +
                ", latitude=" + latitude + ", latitudeDirection=" + latitudeDirection +
                ", longitude=" + longitude + ", longitudeDirection=" + longitudeDirection +
                ", altitude=" + altitude + '}';
    }
}
