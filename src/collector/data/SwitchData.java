package collector.data;

/** Data from a switch (boolean state). */
public class SwitchData implements SensorData {
    private final long timestamp;
    private final boolean state;

    public SwitchData(long timestamp, boolean state) {
        this.timestamp = timestamp;
        this.state = state;
    }

    /** Get the state of the switch. */
    public boolean getState() {
        return state;
    }

    /** Get the state of the switch as a character (0 = open, 1 = closed). */
    public char getStateAsChar() {
        return state ? '1' : '0';
    }

    /** Get the number of seconds since the epoch when this data was collected. */
    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "SwitchData{timestamp=" + timestamp + ", state=" + state + '}';
    }
}
