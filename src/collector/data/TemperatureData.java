package collector.data;

/** Data from a temperature sensor. */
public class TemperatureData implements SensorData {
    private final long timestamp;
    private final double temperature;

    public TemperatureData(long timeStamp, double temperature) {
        timestamp = timeStamp;
        this.temperature = temperature;
    }

    /** Get the temperature in degrees Celsius. */
    public double getTemperature() {
        return temperature;
    }

    /** Get the timestamp for when this data was collected. */
    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "TemperatureData{timestamp=" + timestamp + ", temperature=" + temperature + '}';
    }

    public static double inFahrenheit(TemperatureData data) {
        return (data.getTemperature() * 9.0 / 5.0) + 32.0;
    }
}
