package collector.store.impl;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Logger;
import collector.store.PersistentStore;

/** Persistent store using a file. */
public class FilePersistentStore implements PersistentStore {
    private static final Logger log = Logger.getLogger(FilePersistentStore.class.getName());

    private PrintStream fileWriter;
    private int recordCount;

    public FilePersistentStore(String fileName) throws IOException {

        try {
            String connectorName = "file://" + fileName;
            log.info("constructor opening file: [" + connectorName + "]");
            final File connection = new File(fileName);

            /* If the file does not exist yet, create it */
            if (!connection.exists()) {
                final boolean newFile = connection.createNewFile();
                log.info("File does not exist, " + (newFile ? "new file created" : "file already exists"));
            }

            fileWriter = new PrintStream(connection);
            log.info("Got IO streams");
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public int addRecord(String data) {
        log.info("saveData called");
        fileWriter.println(data);
        return ++recordCount;
    }

    @Override
    public int addRecord(byte[] record, int from, int to) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getRecordCount() {
        return recordCount;
    }

    @Override
    public byte[] getRecord(int id) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void close() throws IOException {
        fileWriter.close();
    }
}
