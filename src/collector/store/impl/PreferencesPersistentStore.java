package collector.store.impl;

import static java.util.prefs.Preferences.userRoot;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import collector.store.InvalidRecordIdException;
import collector.store.PersistentStore;
import collector.store.RecordStoreException;

public class PreferencesPersistentStore implements PersistentStore {
    private static final Logger log = Logger.getLogger(PreferencesPersistentStore.class.getName());
    public static final String PREFERENCES_KEY = PreferencesPersistentStore.class.getName();
    private final String name;

    public static PreferencesPersistentStore openRecordStore(String name, boolean create) {
        return new PreferencesPersistentStore(name, create);
    }

    private final ArrayList<byte[]> store;

    public PreferencesPersistentStore(String name, boolean create) {
        this.name = name;
        final byte[] serialized = userRoot().getByteArray(PREFERENCES_KEY + "[" + this.name + "]", new byte[0]);

        ArrayList<byte[]> loadStore = null;
        if (serialized.length != 0) {
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(serialized);
            final Object deSerialized;
            try {
                deSerialized = new ObjectInputStream(inputStream).readObject();
                if (deSerialized instanceof ArrayList) {
                    // noinspection unchecked
                    loadStore = (ArrayList<byte[]>) deSerialized;
                } else {
                    System.out.println("Invalid state for persistent record store");
                }
            } catch (Exception exception) {
                throw new RecordStoreException("Error initializing record store " + name, exception);
            }
        }
        if (!create && loadStore == null) throw new RecordStoreException("Store " + name + " does not exists");
        if (loadStore == null) loadStore = new ArrayList<>();
        this.store = loadStore;
    }

    @Override
    public int getRecordCount() {
        return store.size();
    }

    @Override
    public byte[] getRecord(int id) {
        final byte[] bytes = store.get(id - 1);
        if (bytes == null) throw new InvalidRecordIdException();
        return bytes;
    }

    @Override
    public synchronized int addRecord(String record) {
        return addRecord(record.getBytes(), 0, record.getBytes().length);
    }

    @Override
    public int addRecord(byte[] record, int from, int to) {
        store.add(Arrays.copyOfRange(record, from, to));
        final int recordNumber = store.size();
        PreferencesPersistentStore.log.fine("saved record number " + recordNumber);
        return recordNumber;
    }

    @Override
    public void close() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final ObjectOutputStream outputStream = new ObjectOutputStream(out);
        outputStream.writeObject(store);
        outputStream.flush();
        userRoot().putByteArray(PREFERENCES_KEY + "[" + name + "]", out.toByteArray());
    }
}
