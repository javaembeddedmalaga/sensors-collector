package collector.store;

public class RecordStoreException extends RuntimeException {
    public RecordStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecordStoreException(String message) {
        super(message);
    }
}
