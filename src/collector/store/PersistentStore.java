package collector.store;

/** Interface for classes that can store persistent GPS data, for example RMS or a file. */
public interface PersistentStore extends AutoCloseable {

    int getRecordCount();

    byte[] getRecord(int id);

    int addRecord(String data);

    int addRecord(byte[] record, int from, int to);

}
