package collector;

import collector.sensor.impl.MPL115A2_Barometer;
import collector.util.Util;

public class MplBarometerPing {
    public static void main(String[] args) {
        Util.commonConfiguration();
        Util.ping(5, new MPL115A2_Barometer(), sensor -> {
            System.out.println(sensor.getPressureData());
            System.out.println(sensor.getTemperatureData());
        });
    }
}
