package collector;

import collector.sensor.impl.MTK3329_Gps;
import collector.util.Util;

public class MtkGpsPing {
    public static void main(String[] args) {
        Util.commonConfiguration();
        Util.ping(5, new MTK3329_Gps(), sensor -> {
            System.out.println(sensor.getPositionData());
            System.out.println(sensor.getVelocityData());
        });
    }
}
