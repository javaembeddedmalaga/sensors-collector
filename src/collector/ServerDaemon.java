package collector;

import collector.server.DataServer;
import collector.store.PersistentStore;
import collector.store.impl.PreferencesPersistentStore;

public class ServerDaemon {
    public static final String STORE_NAME = "event-data";
    public static final int PORT = 8042;

    public static void main(String[] args) throws Exception {
        new ServerDaemon().startApp();
    }

    public void startApp() throws Exception {
        final PersistentStore store = new PreferencesPersistentStore(STORE_NAME, true);
        if (store.getRecordCount() == 0) {
            String[] samples = {
                    "^1389744181,5^^",
                    "^1389747781,-3^^",
                    "^1389751561,7^^",
                    "^1389753606,10^^",
                    "^1389756000,12^^",
                    "^1389758577,8^^",
                    "^^1387202580,40.5583739,N,85.6591442,E,157.25^",
                    "^^1387202967,41.1234567,S,86.1234567,W,135.89^" };
            for (String sample : samples) {
                store.addRecord(sample);
            }
        }

        DataServer server = new DataServer(PORT, store);
        new Thread(server).start();
    }
}
