package collector;

import java.util.logging.Level;
import java.util.logging.Logger;
import collector.store.PersistentStore;
import collector.store.impl.PreferencesPersistentStore;

/** Simple daemon to read and print data stored in the RMS */
public class ReaderDaemon {

    private final static Logger log = Logger.getLogger(ReaderDaemon.class.getName());

    public static void main(String[] args) {
        new ReaderDaemon().startApp();
    }

    public void startApp() {
        try {
            log.info("Opening RMS");
            PersistentStore store = new PreferencesPersistentStore("event-data", true);
            log.info("RMS has " + store.getRecordCount() + " records");
            final int count = store.getRecordCount();
            for (int i = 0; i < count; i++) {
                System.out.println(new String(store.getRecord(i)));
            }
            log.info("Done");
        } catch (RuntimeException ex) {
            log.log(Level.SEVERE, "Unable to access RMS", ex);
        }
    }

}
