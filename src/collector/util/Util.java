package collector.util;

import java.util.function.Consumer;

public class Util {
    public static void commonConfiguration() {
        System.setProperty("jdk.dio.registry", "/root/dio.properties");
        System.setProperty("java.security.policy", "/root/dio.policy");
        System.setProperty("deviceaccess.uart.prefix", "/dev/ttyAMA");
    }

    public static <T extends AutoCloseable> void ping(int times, T sensor, Consumer<T> consume) {
        try (final T s = sensor) {
            int i = 0;
            while (i < times) {
                Thread.sleep(1000);
                consume.accept(s);
                i++;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
