package collector.util;

import java.io.BufferedReader;

public interface HasBufferedReader extends AutoCloseable {
    BufferedReader asBufferedReader();
}
