package collector.sensor;

import collector.data.PressureData;

public interface PressureSensor {
    /** Get the current pressure in hector Pascal units. */
    public PressureData getPressureData();
}
