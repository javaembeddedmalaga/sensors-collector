package collector.sensor;

import collector.data.SwitchData;

public interface SwitchSensor {
    /** Get the switch data (timestamp and state). */
    public SwitchData getSwitchData();
}
