package collector.sensor;

import collector.data.TemperatureData;

public interface TemperatureSensor {
    /** Get the temperature data (timestamp and value). */
    public TemperatureData getTemperatureData();
}
