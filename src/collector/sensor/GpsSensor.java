package collector.sensor;

import collector.data.PositionData;
import collector.data.VelocityData;
import java.io.IOException;

/** Interface for a GPS sensor. */
public interface GpsSensor extends AutoCloseable {
    /** Get the current position. */
    public PositionData getPositionData() throws IOException;

    /** Get the current velocity (bearing and speed). */
    public VelocityData getVelocityData() throws IOException;
}
