package collector.sensor.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.Channels;
import jdk.dio.DeviceManager;
import jdk.dio.uart.UART;
import collector.util.HasBufferedReader;

/** AdaFruit GPS sensor accessed through the UART interface of the device IO API. */
public class BufferedUart implements HasBufferedReader {

    private static final int UART_DEVICE_ID = 40;

    private final BufferedReader bufferedReader;
    private final UART uart;

    public BufferedUart() {
        try {
            uart = DeviceManager.open(UART_DEVICE_ID);
            uart.setBaudRate(9600);

            final InputStream inputStream = Channels.newInputStream(uart);
            final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            this.bufferedReader = new BufferedReader(inputStreamReader);

            System.out.println("AdaFruit GPS Sensor: DIO API UART opened");
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public BufferedReader asBufferedReader() {
        return bufferedReader;
    }

    @Override
    public void close() throws IOException {
        bufferedReader.close();
        uart.close();
    }
}
