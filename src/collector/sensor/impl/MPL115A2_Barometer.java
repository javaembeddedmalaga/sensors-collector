package collector.sensor.impl;

import collector.data.PressureData;
import collector.data.TemperatureData;
import collector.sensor.PressureSensor;
import collector.sensor.TemperatureSensor;

public class MPL115A2_Barometer extends I2CSensor implements PressureSensor, TemperatureSensor {

    private static final byte ADDRESS = 0x60;

    //@formatter:off
    private static final byte REGISTER_PRESSURE_MSB    = 0x00;
    private static final byte REGISTER_PRESSURE_LSB    = 0x01;
    private static final byte REGISTER_TEMP_MSB        = 0x02;
    private static final byte REGISTER_TEMP_LSB        = 0x03;
    private static final byte REGISTER_A0_COEFF_MSB    = 0x04;
    private static final byte REGISTER_A0_COEFF_LSB    = 0x05;
    private static final byte REGISTER_B1_COEFF_MSB    = 0x06;
    private static final byte REGISTER_B1_COEFF_LSB    = 0x07;
    private static final byte REGISTER_B2_COEFF_MSB    = 0x08;
    private static final byte REGISTER_B2_COEFF_LSB    = 0x09;
    private static final byte REGISTER_C12_COEFF_MSB   = 0x0A;
    private static final byte REGISTER_C12_COEFF_LSB   = 0x0B;
    private static final byte REGISTER_STARTCONVERSION = 0x12;

    // From the section 3.1 table                            address               bits
    //                                                               total   sign  frac  dec.zero.pad
    public static Register REGISTER_PADC = new Register("Padc", 0x00,   10,     0,    0,            0);
    public static Register REGISTER_TADC = new Register("Tadc", 0x02,   10,     0,    0,            0);
    public static Register REGISTER_A0   = new Register("A0",   0x04,   16,     1,    3,            0);
    public static Register REGISTER_B1   = new Register("B1",   0x06,   16,     1,   13,            0);
    public static Register REGISTER_B2   = new Register("B2",   0x08,   16,     1,   14,            0);
    public static Register REGISTER_C12  = new Register("C12",  0x0A,   14,     1,   13,            9);
    //@formatter:on

    private final double a0;
    private final double b1;
    private final double b2;
    private final double c12;

    public MPL115A2_Barometer() {
        super(ADDRESS);

        // Read coefficients
        this.a0 = REGISTER_A0.read(this);
        this.b1 = REGISTER_B1.read(this);
        this.b2 = REGISTER_B2.read(this);
        this.c12 = REGISTER_C12.read(this);
    }

    /** Returns a tuple of (pressure, temperature) as measured by the sensor. */
    public Sample getSample() {
        // Instruct the sensor to begin data conversion.
        writeOneByte(REGISTER_STARTCONVERSION, REGISTER_PRESSURE_MSB);

        // Wait until conversion has finished. The datasheet says "3ms". We'll wait 5ms just to be sure.
        try {
            Thread.sleep(5);
        } catch (InterruptedException ignore) {}

        // Read the raw values.
        double padc = REGISTER_PADC.read(this);
        double tadc = REGISTER_TADC.read(this);

        // Calculate compensated pressure value (Pcomp = a0 + (b1 + c12 * Tadc) + Padc * b(c))
        double pcomp = a0 + (b1 + c12 * tadc) * padc + b2 * tadc;

        /*
         * Calculate final values. The formula for pressure is from section 3.2 of the datasheet. The formula for
         * temperature is basically magic: http://www.adafruit.com/forums/viewtopic.php?f=19&t=41347
         */
        double pressure = pcomp * ((115. - 50.) / 1023.) + 50.;
        double temperature = (tadc - 498.) / -5.35 + 25.;

        return new Sample(pressure, temperature);
    }

    /** Pressure in kPa. */
    public double getPressure() {
        return getSample().getPressure();
    }

    /** Temperature in C. */
    public double getTemperature() {
        return getSample().getTemperature();
    }

    @Override
    public PressureData getPressureData() {
        return new PressureData(System.currentTimeMillis(), getPressure());
    }

    @Override
    public TemperatureData getTemperatureData() {
        return new TemperatureData(System.currentTimeMillis(), getTemperature());
    }

    public static class Sample {
        private final double pressure;
        private final double temperature;

        public Sample(double pressure, double temperature) {
            this.pressure = pressure;
            this.temperature = temperature;
        }

        public double getPressure() {
            return pressure;
        }

        public double getTemperature() {
            return temperature;
        }

        @Override
        public String toString() {
            return "Sample{" + "pressure=" + pressure + ", temperature=" + temperature + '}';
        }
    }

}
