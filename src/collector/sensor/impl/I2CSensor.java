package collector.sensor.impl;

import static java.lang.Integer.toHexString;

import java.io.IOException;
import java.nio.ByteBuffer;
import jdk.dio.DeviceManager;
import jdk.dio.i2cbus.I2CDevice;
import jdk.dio.i2cbus.I2CDeviceConfig;

public abstract class I2CSensor implements AutoCloseable {

    private I2CDevice myDevice = null; // I2C device

    private int i2cBus = 1; // Default I2C bus

    private int serialClock = 3400000; // default clock 3.4MHz Max clock
    private int addressSizeBits = 7; // default address
    private int address = 0x77; // I2C device address
    private final int registrySize = 1; // Registry size in bytes

    private ByteBuffer command;
    private ByteBuffer byteToRead;

    /** Constructor for the I2C sensor. This method update the device address and try to connect to the device. */
    public I2CSensor(int address) {
        this.address = address;
        connectToDevice();
    }

    /**
     * Constructor for the I2C sensor.
     * 
     * @param i2cBus Device bus. For Raspberry Pi usually it's 1
     * @param address Device address
     * @param addressSizeBits I2C normally uses 7 bits addresses
     * @param serialClock Clock speed
     */
    public I2CSensor(int i2cBus, int address, int addressSizeBits, int serialClock) {
        this.address = address;
        this.addressSizeBits = addressSizeBits;
        this.serialClock = serialClock;
        this.i2cBus = i2cBus;
        connectToDevice();
    }

    /** This method tries to connect to the I2C device, initializing myDevice variable */
    private void connectToDevice() {
        /*
         * Construct the shared ByteBuffers Reusing the buffers rather than allocating new space each time is good
         * practice with embedded devices to reduce garbage collection.
         */
        command = ByteBuffer.allocateDirect(registrySize);
        byteToRead = ByteBuffer.allocateDirect(1);
        try {
            if (myDevice == null) {
                I2CDeviceConfig config = new I2CDeviceConfig(i2cBus, address, addressSizeBits, serialClock);
                myDevice = DeviceManager.open(config);
            }
            System.out.println("Connected to the device OK!!!");
        } catch (IOException e) {
            System.out.println("Error connecting to device");
        }
    }

    /** This method tries to write one single byte to particular registry. */
    public void writeOneByte(int registry, byte byteToWrite) {
        command.clear();
        command.put(byteToWrite);
        command.rewind();
        try {
            myDevice.write(registry, registrySize, command);
        } catch (IOException e) {
            System.out.println("Error writing registry " + registry);
        }

    }

    public int read(int registry, int registrySize, ByteBuffer byteToRead) {
        try {
            return myDevice.read(registry, registrySize, byteToRead);
        } catch (IOException e) {
            throw new RuntimeException("Error reading registry " + toHexString(registry), e);
        }
    }

    /**
     * This method reads one byte from a specified registry address. The method checks that the byte is actually read,
     * otherwise it'll show some messages in the output
     * 
     * @param registry Registry to be read
     * @return Byte read from the registry
     */
    public byte readOneByte(int registry) {
        byteToRead.clear();
        try {
            int result = myDevice.read(registry, registrySize, byteToRead);
            if (result < 1) throw new RuntimeException("Byte could not be read");
            byteToRead.rewind();
            return byteToRead.get();
        } catch (IOException e) {
            throw new RuntimeException("Error reading registry " + toHexString(registry));
        }
    }

    public short readOneWord(int registry) {
        int result = -1;
        ByteBuffer wordToRead = ByteBuffer.allocateDirect(2);
        try {
            result = myDevice.read(registry, registrySize, wordToRead);
        } catch (IOException e) {
            System.out.println("Error reading word");
        }
        if (result < 1) {
            System.out.println("Word could not be read");
        } else {
            wordToRead.rewind();
            return wordToRead.getShort();
        }
        return 0;
    }

    protected I2CDevice getDevice() {
        return myDevice;
    }

    /** Gracefully close the open I2CDevice */
    public void close() throws IOException {
        myDevice.close();
    }

    public static class Register {
        private final String name;
        private final int address;
        private final int totalBits;
        private final int signBits;
        private final int fractionalBits;
        private final int decimalZeroPadding;

        /**
         * Each register is two bytes wide, with the values left-aligned in the register.
         *
         * @param totalBits gives the total number of data bits (and is used to calculate how much right shifting is
         *            needed to eliminate the padding).
         * @param signBits is the number of bits reserved for sign information and determines whether the value is read
         *            as a signed or unsigned number.
         * @param fractionalBits gives the number of bits that should be considered to follow the decimal point.
         * @param decimalZeroPadding gives the number of additional zero bits that should be considered to exist between
         *            the decimal point and the first data bit.
         */
        public Register(String name, int address, int totalBits, int signBits, int fractionalBits,
                int decimalZeroPadding) {
            this.name = name;
            this.address = address;
            this.totalBits = totalBits;
            this.signBits = signBits;
            this.fractionalBits = fractionalBits;
            this.decimalZeroPadding = decimalZeroPadding;
        }

        public double read(I2CSensor i2c) {
            // Get the data from the register
            final short rawValue = i2c.readOneWord(this.address);
            final double apply = apply(rawValue);
            System.out.println(String.format("read(%1$3s): %2$10.5f [0x%3$x, %3$d]", name, apply, rawValue));
            return apply;
        }

        public double apply(int rawValue) {
            // Negative values are stored in two's-complement
            if (signBits == 1 && (rawValue & 0x8000) != 0) rawValue = rawValue ^ 0xFFFF * -1;

            // Apply the bit parameters
            return ((double) (rawValue >> (16 - totalBits))) / Math.pow(2, (fractionalBits + decimalZeroPadding));
        }

        /* Power method with base x and power y (i.e., x^y) */
        int pow(int x, int y) {
            int z = x;
            for (int i = 1; i < y; i++)
                z *= x;
            return z;
        }
    }
}
