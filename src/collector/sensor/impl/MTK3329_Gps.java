package collector.sensor.impl;

import static java.lang.Double.parseDouble;
import static java.lang.System.currentTimeMillis;

import collector.data.PositionData;
import collector.data.VelocityData;
import collector.sensor.GpsSensor;
import collector.util.HasBufferedReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Common code for the AdaFruit Ultimate GPS sensor. This is an abstract class so the implementation of the readDataLine
 * method can be implemented in subclasses that can use different methods to communicate with the GPS receiver.
 */
public class MTK3329_Gps implements GpsSensor {

    private static final String POSITION_TAG = "GPGGA";
    private static final String VELOCITY_TAG = "GPVTG";

    private final HasBufferedReader hasBufferedReader;

    public MTK3329_Gps() {
        this(new BufferedUart());
    }

    public MTK3329_Gps(HasBufferedReader hasBufferedReader) {
        this.hasBufferedReader = hasBufferedReader;
    }

    private BufferedReader getReader() {
        return hasBufferedReader.asBufferedReader();
    }

    /** Get a line of raw data from the GPS sensor */
    protected String readDataLine() {
        try {
            // All data lines start with a '$' character so keep reading until we find a valid line of data
            while (true) {
                String dataLine = null;
                dataLine = getReader().readLine();
                if (dataLine.startsWith("$")) return dataLine; // Return what we got
            }
        } catch (IOException e) {
            throw new RuntimeException("IO error reading GPS", e);
        }
    }

    /** Get a string of raw data from the GPS receiver. How this happens is sub-class dependent. */
    public String[] getRawData(String type) {
        // Read continuously from the device until type is matched
        while (true) {
            final String dataLine = readDataLine();

            // Extract the type of the data - past the leading $
            final String dataType = dataLine.substring(1).split(",", 2)[0];

            // Compare the type extracted from the string with the type passed in
            // If there is match, break out of the loop
            if (type.equals(dataType)) {
                // Return only the substring that starts at character 7
                final String rawFields = dataLine.substring(7);
                if (rawFields.contains("$GP")) continue; // Corrupt data
                return rawFields.split(",");
            }
        }
    }

    /** Get the current position */
    @Override
    public PositionData getPositionData() {
        final String[] rawFields = getRawData(POSITION_TAG);
        try {
            return new PositionData(currentTimeMillis() / 1000, // timestamp
                    parseDouble(rawFields[1]), // latitude
                    rawFields[2].charAt(0), // latitude direction
                    parseDouble(rawFields[3]), // longitude
                    rawFields[4].charAt(0), // longitude direction
                    parseDouble(rawFields[8])); // altitude
        } catch (Exception parseError) {
            System.err.println("Invalid gps position: " + Arrays.toString(rawFields));
            return null;
        }
    }

    /** Get the current velocity */
    @Override
    public VelocityData getVelocityData() {
        String[] rawFields = getRawData(VELOCITY_TAG);
        try {
            return new VelocityData(currentTimeMillis(), // timestamp
                    parseDouble(rawFields[0]), // bearing
                    parseDouble(rawFields[6])); // speed
        } catch (Exception parseError) {
            System.err.println("Invalid gps position: " + Arrays.toString(rawFields));
            return null;
        }
    }

    @Override
    public void close() throws Exception {
        this.hasBufferedReader.close();
    }
}
