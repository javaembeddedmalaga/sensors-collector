package collector.sensor.impl;

import collector.data.PressureData;
import collector.data.TemperatureData;
import collector.sensor.PressureSensor;
import collector.sensor.TemperatureSensor;
import java.io.IOException;
import java.nio.ByteBuffer;

public class BMP180_Barometer extends I2CSensor implements TemperatureSensor, PressureSensor {

    /** Temperature read address */
    private static final int tempAddr = 0xF6;
    /** Read temperature command */
    private static final byte getTempCmd = (byte) 0x2E;
    /** Temperature read address */
    private static final int pressAddr = 0xF6;
    /** Shared ByteBuffers */
    protected ByteBuffer uncompTemp;
    /** Calibration data only needs to be read once This flag determines if the calibration data was already read once */
    private boolean calibrationDataRead = false;
    /** Device address BMP180 address is 0x77 */
    private static final int BMP180_ADDR = 0x77;

    // EEPROM registers - these represent calibration data
    protected short AC1;
    protected short AC2;
    protected short AC3;
    protected int AC4;
    protected int AC5;
    protected int AC6;
    protected short B1;
    protected short B2;
    protected short MB;
    protected short MC;
    protected short MD;

    // Variable common between temperature & pressure calculations
    protected int B5;

    // Total of bytes use for callibration
    protected final int callibrationBytes = 22;

    // Address byte length
    protected final int subAddressSize = 1; // Size of each address (in bytes)
    // EEPROM address data
    protected final int EEPROM_start = 0xAA;
    // BMP180 control registry
    protected final int controlRegister = 0xF4;
    // Barometer configuration
    private byte pressureCmd;
    private int delay;
    private int oss;

    /**
     * BMP180 constructor invokes the parent constructor to initialize the connection with the I2C device. Them we
     * proceed with some initialization steps(reading calibration data)
     */
    public BMP180_Barometer() {
        this(BarometerModes.STANDARD);
    }

    /**
     * BMP180 constructor invokes the parent constructor to initialize the connection with the I2C device. Them we
     * proceed with some initialization steps(reading calibration data)
     */
    public BMP180_Barometer(BarometerModes mode) {
        super(BMP180_ADDR);
        initDevice();
        uncompTemp = ByteBuffer.allocateDirect(2);
        initCommands(mode);
    }

    /**
     * BMP180 constructor invokes the parent constructor to initialize the connection with the I2C device. Them we
     * proceed with some initialization steps(reading calibration data)
     * 
     * @param i2cBus I2C Bus. For Raspberry Pi it's normally 1
     * @param address Device Address
     * @param addressSizeBits I2C uses 7bits address size
     * @param serialClock Clock speed
     */
    public BMP180_Barometer(int i2cBus, int address, int addressSizeBits, int serialClock, BarometerModes mode) {
        super(i2cBus, address, addressSizeBits, serialClock);
        initDevice();
        uncompTemp = ByteBuffer.allocateDirect(2);
        initCommands(mode);
    }

    /**
     * This method read the calibration data common for the Temperature sensor and Barometer sensor included in the
     * BMP180
     */
    private void initDevice() {
        if (!calibrationDataRead) {
            try {
                // Small delay before starting
                Thread.sleep(500);
                // Getting calibration data
                gettingCalibration();
                calibrationDataRead = true;
            } catch (IOException | InterruptedException e) {
                System.out.println(e.toString());
            }
        }
    }

    /**
     * Method for reading the calibration data. Do not worry too much about this method. Normally this information is
     * given in the device information sheet.
     */
    public void gettingCalibration() throws IOException {
        // Read all of the calibration data into a byte array
        ByteBuffer calibData = ByteBuffer.allocateDirect(callibrationBytes);
        int result = getDevice().read(EEPROM_start, subAddressSize, calibData);
        if (result < callibrationBytes) {
            System.out.println("Not all the callibration bytes were read");
            return;
        }
        // Read each of the pairs of data as a signed short
        calibData.rewind();
        AC1 = calibData.getShort();
        AC2 = calibData.getShort();
        AC3 = calibData.getShort();

        // Unsigned short values
        byte[] data = new byte[2];
        calibData.get(data);
        AC4 = (((data[0] << 8) & 0xFF00) + (data[1] & 0xFF));
        calibData.get(data);
        AC5 = (((data[0] << 8) & 0xFF00) + (data[1] & 0xFF));
        calibData.get(data);
        AC6 = (((data[0] << 8) & 0xFF00) + (data[1] & 0xFF));

        // Signed sort values
        B1 = calibData.getShort();
        B2 = calibData.getShort();
        MB = calibData.getShort();
        MC = calibData.getShort();
        MD = calibData.getShort();
    }

    /** Set the pressure command and delays depending on the selected barometer mode */
    protected void initCommands(BarometerModes mode) {
        pressureCmd = mode.getCommand();
        delay = mode.getDelay();
        oss = mode.getOSS();
    }

    /** Method for reading the temperature and barometric pressure from the BMP device as metric values. */
    public double[] getMetricTemperatureBarometricPressure() {
        double[] result = new double[2];
        result[0] = getTemperatureInCelsius();
        result[1] = getPressureInHPa();
        return result;
    }

    /** Method for reading the temperature and barometric pressure from the BMP device as English values. */
    public double[] getEnglishTemperatureBarometricPressure() {
        double[] result = new double[2];
        result[0] = getTemperatureInFahrenheit();
        result[1] = getPressureInInchesMercury();
        return result;
    }

    /**
     * Method for reading the temperature. Remember the sensor will provide us with raw data, and we need to transform
     * in some analyzed value to make sense. All the calculations are normally provided by the manufacturer. In our case
     * we use the calibration data collected at construction time.
     */
    public double getTemperatureInCelsius() {
        // Write the read temperature command to the command register
        writeOneByte(controlRegister, getTempCmd);

        // Delay before reading the temperature
        try {
            Thread.sleep(100);
        } catch (InterruptedException ignored) {}

        // Read uncompressed data
        uncompTemp.clear();
        int result = read(tempAddr, subAddressSize, uncompTemp);
        if (result < 2) {
            System.out.println("Not enough data for temperature read");
        }

        // Get the uncompensated temperature as a signed two byte word
        uncompTemp.rewind();
        byte[] data = new byte[2];
        uncompTemp.get(data);
        int UT = ((data[0] << 8) & 0xFF00) + (data[1] & 0xFF);

        // Calculate the actual temperature
        int X1 = ((UT - AC6) * AC5) >> 15;
        int X2 = (MC << 11) / (X1 + MD);
        B5 = X1 + X2;
        float celsius = (float) ((B5 + 8) >> 4) / 10;

        return celsius;
    }

    /** Read the temperature value as a Celsius value and the convert the value to Fahrenheit. */
    public double getTemperatureInFahrenheit() {
        return celsiusToFahrenheit(getTemperatureInCelsius());
    }

    /** Calculate temperature in Fahrenheit based on a celsius temp. */
    public double celsiusToFahrenheit(double temp) {
        return ((temp * 1.8) + 32);
    }

    /** Read the barometric pressure (in hPa) from the device. */
    public double getPressureInHPa() {

        // Write the read pressure command to the command register
        writeOneByte(controlRegister, pressureCmd);

        // Delay before reading the pressure - use the value determined by the oversampling setting (mode)
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ignored) {}

        // Read the uncompensated pressure value
        ByteBuffer unCompPress = ByteBuffer.allocateDirect(3);
        int result;
        result = read(pressAddr, subAddressSize, unCompPress);
        if (result < 3) {
            System.out.println("Couldn't read all bytes, only read = " + result);
            return 0;
        }

        // Get the uncompensated pressure as a three byte word
        unCompPress.rewind();

        byte[] data = new byte[3];
        unCompPress.get(data);

        int UP = ((((data[0] << 16) & 0xFF0000) + ((data[1] << 8) & 0xFF00) + (data[2] & 0xFF)) >> (8 - oss));

        // Calculate the true pressure
        int B6 = B5 - 4000;
        int X1 = (B2 * (B6 * B6) >> 12) >> 11;
        int X2 = AC2 * B6 >> 11;
        int X3 = X1 + X2;
        int B3 = ((((AC1 * 4) + X3) << oss) + 2) / 4;
        X1 = AC3 * B6 >> 13;
        X2 = (B1 * ((B6 * B6) >> 12)) >> 16;
        X3 = ((X1 + X2) + 2) >> 2;
        int B4 = (AC4 * (X3 + 32768)) >> 15;
        int B7 = (UP - B3) * (50000 >> oss);

        int Pa;
        if (B7 < 0x80000000) {
            Pa = (B7 * 2) / B4;
        } else {
            Pa = (B7 / B4) * 2;
        }

        X1 = (Pa >> 8) * (Pa >> 8);
        X1 = (X1 * 3038) >> 16;
        X2 = (-7357 * Pa) >> 16;

        Pa += ((X1 + X2 + 3791) >> 4);

        return (Pa) / 100;
    }

    @Override
    public PressureData getPressureData() {
        return new PressureData(System.currentTimeMillis(), getPressureInHPa());
    }

    /** Read the barometric pressure (in inches mercury) from the device. */
    public double getPressureInInchesMercury() {
        return pascalToInchesMercury(getPressureInHPa());
    }

    /**
     * Calculate pressure in inches of mercury (inHg) - uses a constant to convert between the two
     * 
     * @param pressure - The pressure in hPa
     * @return float - Pressure converted to inches Mercury (inHg)
     */
    public double pascalToInchesMercury(double pressure) {
        return (pressure * 0.0296);
    }

    @Override
    public TemperatureData getTemperatureData() {
        return new TemperatureData(System.currentTimeMillis(), getTemperatureInCelsius());
    }

    public static enum BarometerModes {
        // Relationship between Oversampling Setting and conversion delay (in ms) for each Oversampling Setting
        // contstant
        // Ultra low power: 4.5 ms minimum conversion delay
        // Standard: 7.5 ms
        // High Resolution: 13.5 ms
        // Ultra high Resolution: 25.5 ms

        ULTRA_LOW_POWER(0, 5), STANDARD(1, 8), HIGH_RESOLUTION(2, 14), ULTRA_HIGH_RESOLUTION(3, 26);

        private final int oss; // Oversample setting value
        private final int delay; // Minimum conversion time in ms
        private static final byte getPressCmd = (byte) 0x34; // Read pressure command
        private final byte cmd; // Command byte to read pressure

        /** Barometer Modes constructor. */
        BarometerModes(int oss, int delay) {
            this.oss = oss;
            this.delay = delay;
            this.cmd = (byte) (getPressCmd + ((oss << 6) & 0xC0));
        }

        /** Return the conversion delay (in ms) associated with this oversampling setting */
        public int getDelay() {
            return delay;
        }

        /** Return the command to the control register for this oversampling setting */
        public byte getCommand() {
            return cmd;
        }

        /** Return this oversampling setting */
        public int getOSS() {
            return oss;
        }
    }
}
