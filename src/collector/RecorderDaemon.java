package collector;

import java.util.ArrayList;
import java.util.List;
import collector.data.*;

/**
 * MIDlet for getting data from the sensors via the Raspberry Pi ports and storing it in the RMS and a file. Note that
 * writing to the file is just for an example, but that we're getting records only from the RMS when responding to
 * client requests
 */
public class RecorderDaemon {



    private static final String RECORD_TYPE_DELIMITER = "^";
    private final List<SwitchData> switchStates;
    private final List<TemperatureData> temperatureDatas;
    private final List<PositionData> positionDatas;
    private final List<VelocityData> velocities;

    public RecorderDaemon() {
        switchStates = new ArrayList();
        temperatureDatas = new ArrayList();
        positionDatas = new ArrayList();
        velocities = new ArrayList();
    }

    public void startApp() {
//        System.out.println("RecorderMIDlet: starting");
//
//        try (DoorSensor doorSensor = new DoorSensor(0, 17, 24, 23);
//                MPL115A2_Barometer tempSensor = new MPL115A2_Barometer();
//                GpsSensor gps = Sensors.createGps();
//                FilePersistentStore fileStore = new FilePersistentStore("/rootfs/tmp/event-data.txt");
//                RMSPerisistentStore rmsStore = new RecStor("event-data")) {
//
//            // Comment out the setMessageLevel method invocations to reduce messages
//            // doorSensor.setMessageLevel(INFO);
//            tempSensor.setMessageLevel(INFO);
//            fileStore.setMessageLevel(INFO);
//            rmsStore.setMessageLevel(INFO);
//            gps.setMessageLevel(INFO);
//            doorSensor.start();
//
//            /*
//             * As an example we'll take one reading from each sensor every second, storing them in the RMS and a file
//             */
//            System.out.println("RecorderMIDlet: reading sensors");
//            for (int i = 0; i < 10; i++) {
//                SwitchData sd = doorSensor.getSwitchData();
//                switchStates.add(sd);
//
//                TemperatureData td = tempSensor.getTemperatureData();
//                temperatures.add(td);
//
//                Position p = gps.getPosition();
//                positions.add(p);
//
//                Velocity v = gps.getVelocity();
//                velocities.add(v);
//
//                Thread.sleep(1000);
//            }
//
//            System.out.println("RecorderMIDlet: saving data");
//            String dataStr = "";
//
//            for (int i = 0; i < 10; i++) {
//                dataStr = switchStates.get(i).toString();
//                dataStr += RECORD_TYPE_DELIMITER;
//                dataStr += temperatures.get(i).toString();
//                dataStr += RECORD_TYPE_DELIMITER;
//                dataStr += positions.get(i).toString();
//                dataStr += RECORD_TYPE_DELIMITER;
//                dataStr += velocities.get(i).toString();
//                fileStore.saveData(dataStr);
//                rmsStore.saveData(dataStr);
//            }
//
//            System.out.println("RecorderMIDlet finished");
//        } catch (IOException ioe) {
//            System.out.println("RecorderMidlet: IOException " + ioe.getMessage());
//            notifyDestroyed();
//        } catch (InterruptedException ex) {
//            // Ignore
//        }
    }

}
