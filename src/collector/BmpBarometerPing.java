package collector;

import collector.sensor.impl.BMP180_Barometer;
import collector.util.Util;

public class BmpBarometerPing {
    public static void main(String[] args) {
        Util.commonConfiguration();
        Util.ping(5, new BMP180_Barometer(), sensor -> {
            System.out.println(sensor.getTemperatureData());
            System.out.println(sensor.getPressureData());
        });
    }
}
