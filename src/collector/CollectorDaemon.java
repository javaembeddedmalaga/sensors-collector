package collector;

public class CollectorDaemon {
    public static void main(String[] args) throws Exception {
        new RecorderDaemon().startApp();
        new ReaderDaemon().startApp();
        new ServerDaemon().startApp();
    }
}
