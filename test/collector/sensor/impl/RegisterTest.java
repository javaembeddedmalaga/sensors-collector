package collector.sensor.impl;

import static collector.sensor.impl.MPL115A2_Barometer.*;
import static collector.util.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class RegisterTest {
    //@formatter:off

    // a0 coefficient MSB = 0x3E | a0 coefficient LSB = 0xCE | a0 coefficient = 0x3ECE = 2009.75
    @Test public void testA0() { assertThat(REGISTER_A0.apply(0x3ECE), closeTo(2009.75, .001)); }

    // b1 coefficient MSB = 0xB3 | b1 coefficient LSB = 0xF9 | b1 coefficient = 0xB3F9 = -2.37585
    @Test public void testB1() { assertThat(REGISTER_B1.apply(0xB3F9), closeTo(-2.37585, .001)); }

    // b2 coefficient MSB = 0xC5 | b2 coefficient LSB = 0x17 | b2 coefficient = 0xC517 = -0.92047
    @Test public void testB2() { assertThat(REGISTER_B2.apply(0xC517), closeTo(-0.92047, .001)); }

    // c12 coefficient MSB = 0x33 | c12 coefficient LSB = 0xC8 | c12 coefficient = 0x33C8 = 0.000790
    @Test public void testC12() { assertThat(REGISTER_C12.apply(0x33C8), closeTo(0.000790, .000001)); }

    // Pressure MSB = 0x66 | Pressure LSB = 0x80 | Pressure = 0x6680 = 0110 0110 1100 0000 = 410 ADC counts
    @Test public void testPADC() { assertThat(REGISTER_PADC.apply(0x6680), closeTo(410, .1)); }

    // Temperature MSB = 0x7E | Temperature LSB = 0xC0 | Temperature = 0x7EC0 = 0111 1110 1100 0000 = 507 ADC counts
    @Test public void testTADC() { assertThat(REGISTER_TADC.apply(0x7EC0), closeTo(507, .1)); }
}
