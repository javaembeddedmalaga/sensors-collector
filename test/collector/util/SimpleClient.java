package collector.util;

import static java.util.Objects.requireNonNull;

import collector.ServerDaemon;
import collector.server.DataConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class SimpleClient {
    public static void main(String[] args) throws Exception {
        final String host = requireNonNull(System.getProperty("serverHost"),
                "simple client requires -DserverHost=?.?.?.?");
        final Socket socket = new Socket(host, ServerDaemon.PORT);
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        final OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream());
        writer.write(DataConnection.READ_DATA);
        writer.flush();

        final String lines = reader.readLine();
        for (int i = 0; i < Integer.valueOf(lines); i++) {
            System.out.println(reader.readLine());
        }

        writer.write(DataConnection.DISCONNECT);
        writer.flush();

        socket.close();
    }

}
